﻿#include<iostream>
#include <SFML/Graphics.hpp>
#include "SomeFunctions.hpp"
#pragma comment (linker,"/SUBSYSTEM:windows /ENTRY:mainCRTStartup")
int main()
{
    //фон
    sf::RenderWindow window(sf::VideoMode(1920, 1080), "igra");
    sf::Texture texture;
    if (!texture.loadFromFile("background/picters.1.jpg"))
    {
        std::cout << "ERROR" << std::endl;
        return -1;
    }
    sf::Sprite background(texture);
    float CurrentFrame = 0;//хранит текущий кадр
    sf::Clock clock;
    dt::Player p("picters2.png", 900, 500);
    dt::Player m("picters3.png", 750, 500);
    while (window.isOpen()) //пока открыто окно
    {
        float time = clock.getElapsedTime().asMicroseconds();
        clock.restart();//перезагружает время
        time = time / 800;//скорость игры
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }
        //движение 
        if (p.y > -10)
        {
            if ((sf::Keyboard::isKeyPressed(sf::Keyboard::Up)))
            {
                p.dir = 3; p.speed = 0.15;//dir =3 - направление вверх, speed =0.15 - скорость движения.
                CurrentFrame += 0.01 * time;//как часто будут передвигаться ноги
                if (CurrentFrame > 3) CurrentFrame -= 3;//проходимся по кадрам с первого по третий включительно. если пришли к третьему кадру - откидываемся назад.
                p.sprite.setTextureRect(sf::IntRect(120 * int(CurrentFrame), 560, 120, 180));  //через объект p класса player меняем спрайт, делая анимацию (используя оператор точку)
            }
        }
        if (p.x < 1810)
        {
            if ((sf::Keyboard::isKeyPressed(sf::Keyboard::Right)))
            {
                p.dir = 0; p.speed = 0.15;
                CurrentFrame += 0.01 * time;
                if (CurrentFrame > 3) CurrentFrame -= 3;
                p.sprite.setTextureRect(sf::IntRect(120 * int(CurrentFrame), 379, 120, 180));
            }
        }
        if (p.x > -20)
        {
            if ((sf::Keyboard::isKeyPressed(sf::Keyboard::Left)))
            {
                p.dir = 1; p.speed = 0.15;
                CurrentFrame += 0.01 * time;
                if (CurrentFrame > 3) CurrentFrame -= 3;
                p.sprite.setTextureRect(sf::IntRect(120 * int(CurrentFrame), 200, 120, 180));
            }
        }
        if (p.y < 900)
        {
            if ((sf::Keyboard::isKeyPressed(sf::Keyboard::Down)))
            {
                p.dir = 2; p.speed = 0.15;
                CurrentFrame += 0.01 * time;
                if (CurrentFrame > 3) CurrentFrame -= 3;
                p.sprite.setTextureRect(sf::IntRect(120 * int(CurrentFrame), 0, 120, 180));
            }
        }
        if (m.x > -20)
        {
            if ((sf::Keyboard::isKeyPressed(sf::Keyboard::A)))
            {
                m.dir = 1; m.speed = 0.15;
                CurrentFrame += 0.01 * time;
                if (CurrentFrame > 3) CurrentFrame -= 3;
                m.sprite.setTextureRect(sf::IntRect(124 * int(CurrentFrame), 190, 120, 180));
            }
        }
        if (m.x < 1810)
        {
            if ((sf::Keyboard::isKeyPressed(sf::Keyboard::D)))
            {
                m.dir = 0; m.speed = 0.15;
                CurrentFrame += 0.01 * time;
                if (CurrentFrame > 3) CurrentFrame -= 3;
                m.sprite.setTextureRect(sf::IntRect(120 * int(CurrentFrame), 379, 120, 180));
            }
        }
        if (m.y > -5)
        {
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
            {
                m.dir = 3; m.speed = 0.15;
                CurrentFrame += 0.01 * time;
                if (CurrentFrame > 3) CurrentFrame -= 3;
                m.sprite.setTextureRect(sf::IntRect(124 * int(CurrentFrame), 560, 120, 180));
            }
        }
        if (m.y < 900)
        {
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
            {
                m.dir = 2; m.speed = 0.15;
                CurrentFrame += 0.01 * time;
                if (CurrentFrame > 3) CurrentFrame -= 3;
                m.sprite.setTextureRect(sf::IntRect(124 * int(CurrentFrame), 0, 120, 180));
            }
        }
        m.update(time);
        p.update(time);//оживляем объект p  с помощью времени sfml
        window.clear();//очищаем 
        window.draw(background);
        window.draw(p.sprite);//рисуем спрайт объекта p класса player
        window.draw(m.sprite);//
        window.display();
    }
    return 0;
}