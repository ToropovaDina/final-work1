#include <SFML/Graphics.hpp>
#include<iostream>
#include "SomeFunctions.hpp"
namespace dt
{
    Player::Player(sf::String F, float X, float Y)
    {
        dir = 0, speed = 0;
        File = F;//��� �����+����������
        //������ � ������
        w = 120;
        h = 180;
        sf::Event event;
        image.loadFromFile("image/" + File);
        texture.loadFromImage(image);//���������� ���� ����������� � ��������
        sprite.setTexture(texture);//�������� ������ ���������
        x = X; y = Y;//���������� ��������� �������
        sprite.setTextureRect(sf::IntRect(0, 0, w, h)); //������ ������� ���� ������������� ��� ������ ������ ���������
    }
    Player::~Player()
    {
    }
    void Player::update(float time)
    {
        switch (dir)//��������� ��������� � ����������� �� �����������
        {
        case 0: m_x = speed; m_y = 0; break;// �������� ���� ������ ������
        case 1: m_x = -speed; m_y = 0; break;// �������� ���� ������ �����
        case 2: m_x = 0; m_y = speed; break;// �������� ���� ������ ����
        case 3: m_x = 0; m_y = -speed; break;// �������� ���� ������ �����
        }
        x += m_x * time;//�������� �������� ��������� � ��� ��������� ��������
        y += m_y * time;
        speed = 0;//�������� ��������, ����� �������� �����������.
        sprite.setPosition(x, y);
    }//������� ������ � ������� x y ,����� ��� ������ ��  ����� �� �����.

}