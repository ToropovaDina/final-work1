#pragma once
#include <SFML/Graphics.hpp>
namespace dt
{
    class Player
    {
    private:
        float w, h, m_x = 0, m_y = 0;
    public:
        float x, y, speed; //���������� ������ � � �,��������� (�� � � �� �), ���� ��������
        int dir = 0;
        sf::String File;
        sf::Texture texture;
        sf::Image image;
        sf::Sprite sprite;
        sf::Clock clock;
        Player(sf::String F, float X, float Y);
        ~Player();
        void update(float time);
    };
}
